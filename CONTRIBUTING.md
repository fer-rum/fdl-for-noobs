## Creating Issues

Everybody is invited to create issues. 
If doing so, please add an apropriate label.
If you want to reference a specific location in the text, 
please refer to the specific commit, source file and line in the source code.
(Keep in mind, that line numbers, definition numbers etc. can change across 
commits, therefor be as specific as possible)

## Pushing changes (additions/ improvements)

Please create your own branch and create a merge request towards _master_.
If you solve an issue with your contribution, please reference the issue during 
the merge request.
Do not forget to add yourself in the _authors.tex_