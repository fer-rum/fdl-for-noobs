\begin{exercise}\label{ex:4.1}
    Extend the reduction for $\zadeh-\ALC{=}$ to concepts of the form $A_{\leq p}$\linebreak (and $\gamma(C, \leq p)$),
    with the intention to represent all domain elements
    that satisfy $A$ (or $C$) to degree $\leq p$,
    where $p \in \val{\zadeh}{\ontology} \setminus \set{1}$.
    \begin{subexercise}
        \item\label{ex:4.1:a}
            Define $\gamma(C, \leq p)$ for all concepts of the forms
            $\top$, $\bottom$, $D \dlAnd E$, $D \dlOr E$, $\dlNot D$, and $D \dlImplication E$.
        \item\label{ex:4.1:b}
            Extend the definition of $\kappa(C \dlGCI D)$ accordingly.
        \item\label{ex:4.1:c}
            Extend $\delta(\ontology)$ to encode the semantics
            of the new concept names $A_{\leq p}$.
    \end{subexercise}
%
    \begin{solution}
        \begin{remark}[Short alternative solution]
            Since $\precomplementZ : x \mapsto 1 - x$ is bijective,
            we can make use of the equivalence
            \begin{align*}
                C^\I(d) \leq p
                &\iff 1 - C^\I(d) \geq 1 - p \\
                &\iff \precomplementZ C^\I(d) \geq 1 - p \\
                &\iff (\dlNot C)^\I(d) \geq 1 - p
            \end{align*}
            and reduce
            $\gamma(C, \leq p)$
            to
            $\gamma(\dlNot C, \geq 1 - p)$
            .
        \end{remark}
%
        \begin{subsolution}
            \item[\ref{ex:4.1:a}]
                \begin{align*}
                    \gamma(A, \leq p)
                    &\definedas
                        A_{\leq p}
                        && A \in \conceptNames \\
                    \gamma(\top, \leq p)
                    &\definedas
                        \bottom \\
                    \gamma(\bottom, \leq p)
                    &\definedas
                        \top \\
                    \gamma(D \dlAnd E, \leq p)
                    &\definedas
                        \gamma(D, \leq p) \dlOr \gamma(E, \leq p) \\
                    \gamma(D \dlOr E, \leq p)
                    &\definedas
                        \gamma(D, \leq p) \dlAnd \gamma(E, \leq p) \\
                    \gamma(\dlNot D, \leq p)
                    &\definedas
                        \gamma(D, \geq 1 - p) \\
                    \gamma(D \dlImplication E, \leq p)
                    &\definedas
                        \gamma(D, \geq 1 - p) \dlAnd \gamma(E, \leq p)
                \end{align*}
%
                \begin{claim}[Soundness of the reduction]
                    Let $\I$ be an arbitrary (crisp) model of $\delta(\ontology) \union \kappa(\ontology)$
                    and $\J$ the corresponding fuzzy interpretation
                    \emph{(as defined in \missingReference{L02s60})}.
                    For all $d \in \domain{\I} = \domain{\J}$,
                    $C \in \subconcepts{\ontology}$,
                    and $p \in \val{\zadeh}{\ontology} \setminus \set{1}$,
                    we have
                    \[
                        C^\J(d) \leq p
                        \iff
                        d \in \gamma(C, \leq p)^\I
                        .
                    \]
                %
                    \begin{proof}
                        \missingText{See issue \#58!}
                    \end{proof}
                \end{claim}
%
                \begin{claim}[Completeness of the reduction]
                    Let $\I$ be an arbitrary (fuzzy) model of the given ontology $\ontology$,
                    and $\J$ the corresponding crisp interpretation
                    \emph{(as defined in \missingReference{L02s61})}.
                    For all $d \in \domain{\I} = \domain{\J}$,
                    $C \in \subconcepts{\ontology}$,
                    and $p \in \val{\zadeh}{\ontology} \setminus \set{1}$,
                    we have
                    \[
                        C^\I(d) \leq p
                        \iff
                        d \in \gamma(C, \leq p)^\J
                        .
                    \]
                %
                    \begin{proof}
                        Induction over the structure of all concepts:
                        \begin{itemize}
                            \item Base cases:
                                \begin{align*}
                                    A^\I(d) \leq p
                                    &\iff d \in (A_{\leq p})^\J
                                        \transformationHintText{%
                                            by definition
                                        } \\
                                    \top^\I(d) \leq p
                                    &\iff d \in \bottom^\J
                                        \transformationHintText{%
                                            $\top^\I(d) = 1 \not\leq p$ for all $d \in \domain{\I} = \top^\J$
                                        } \\
                                    \bottom^\I(d) \leq p
                                    &\iff d \in \top^\J
                                        \transformationHintText{%
                                            $\bottom^\I(d) = 0 \leq p$ for all $d \in \domain{\I} = \top^\J$
                                        }
                                \end{align*}
                            \item Induction hypothesis (IH):
                                Assume that the claim holds for the concepts
                                $D$ and $E$.
                            \item Induction step:
                                \begin{align*}
                                    &(D \dlAnd E)^\I(d) \leq p \\
                                    \iff &\min\set{D^\I(d), E^\I(d)} \leq p
                                        \transformationHintText{%
                                            semantics of fuzzy $\dlAnd$
                                        } \\
                                    \iff &D^\I(d) \leq p \lor E^\I(d) \leq p \\
                                    \iff &d \in \gamma(D, \leq p)^\J \lor d \in \gamma(E, \leq p)^\J
                                        \transformationHintText{(IH)} \\
                                    \iff &d \in \gamma(D, \leq p)^\J \union \gamma(E, \leq p)^\J \\
                                    \iff &d \in \left( \gamma(D, \leq p) \dlOr \gamma(E, \leq p) \right)^\J
                                        \transformationHintText{%
                                            semantics of crisp $\dlOr$
                                        } \\
                                    \iff &d \in \gamma(D \dlAnd E, \leq p)^\J
                                        \transformationHintText{%
                                            by definition
                                        } \\
                                    %
                                    &(D \dlOr E)^\I(d) \leq p \\
                                    \iff &\max\set{D^\I(d), E^\I(d)} \leq p
                                        \transformationHintText{%
                                            semantics of fuzzy $\dlOr$
                                        } \\
                                    \iff &D^\I(d) \leq p \land E^\I(d) \leq p \\
                                    \iff &d \in \gamma(D, \leq p)^\J \land d \in \gamma(E, \leq p)^\J
                                        \transformationHintText{(IH)} \\
                                    \iff &d \in \gamma(D, \leq p)^\J \intersection \gamma(E, \leq p)^\J \\
                                    \iff &d \in \left( \gamma(D, \leq p) \dlAnd \gamma(E, \leq p) \right)^\J
                                        \transformationHintText{%
                                            semantics of crisp $\dlAnd$
                                        } \\
                                    \iff &d \in \gamma(D \dlOr E, \leq p)^\J
                                        \transformationHintText{%
                                            by definition
                                        } \\
                                    %
                                    &(\dlNot D)^\I(d) \leq p \\
                                    \iff &1 - D^\I(d) \leq p
                                        \transformationHintText{%
                                            semantics of fuzzy $\dlNot$
                                        } \\
                                    \iff &D^\I(d) \geq 1 - p \\
                                    \iff &d \in \gamma(D, \geq 1 - p)^\J
                                        \transformationHintText{%
                                            \missingReference{Claim L02s61}
                                        } \\
                                    \iff &d \in \gamma(\dlNot D, \leq p)^\J
                                        \transformationHintText{%
                                            by definition
                                        } \\
                                    %
                                    &(D \dlImplication E)^\I(d) \leq p \\
                                    \iff &\max\set{1 - D^\I(d), E^\I(d)} \leq p
                                        \transformationHintText{%
                                            semantics of fuzzy $\dlImplication$
                                        } \\
                                    \iff &1 - D^\I(d) \leq p \land E^\I(d) \leq p \\
                                    \iff &D^\I(d) \geq 1 - p \land E^\I(d) \leq p \\
                                    \iff &d \in \gamma(D, \geq 1 - p)^\J \land d \in \gamma(E, \leq p)^\J
                                        \transformationHintText{%
                                            (IH) + \missingReference{Claim l02s61}
                                        } \\
                                    \iff &d \in \gamma(D, \geq 1 - p)^\J \intersection \gamma(E, \leq p)^\J \\
                                    \iff &d \in \left( \gamma(D, \geq 1 - p) \dlAnd \gamma(E, \leq p) \right)^\J
                                        \transformationHintText{%
                                            semantics of crisp $\dlAnd$
                                        } \\
                                    \iff &d \in \gamma(D \dlImplication E, \leq p)^\J
                                        \transformationHintText{%
                                            by definition
                                        }
                                    \qedhere
                                \end{align*}
                        \end{itemize}
                    \end{proof}
                \end{claim}
            \item[\ref{ex:4.1:b}]
                \begin{align*}
                    \kappa(C \dlGCI D)
                    \definedas
                    &\phantom{\union}\ %
                    \bigUnion_{
                            \substack{
                                \text{all $\ggeq p$ except} \\
                                \text{$\geq 0$ and $> 1$}
                            }
                        }
                        \set{
                            \gamma(C, \ggeq p)
                            \dlGCI
                            \gamma(D, \ggeq p)
                        } \\
                    &\union
                    \bigUnion_{
                            \text{all $p$ except $1$}
                        }
                        \set{
                            \gamma(D, \leq p)
                            \dlGCI
                            \gamma(C, \leq p)
                        }
                \end{align*}
            \item[\ref{ex:4.1:c}]
                \begin{align*}
                    \delta(\ontology)
                    \definedas
                    &\phantom{\union}\ %
                    \setCondition{
                        \beta_{\geq p^+} \dlGCI \beta_{> p},
                        \beta_{> p} \dlGCI \beta_{\geq p},
                        \beta_{\geq 0^+} \dlGCI \beta_{> 0}
                    }{
                        p \in \val{\zadeh}{\ontology} \setminus \set{0, 1},
                        \beta \in \conceptNames(\ontology) \union \roleNames(\ontology)
                    } \\
                    &\union
                    \setCondition{
                        \beta_{\leq p} \dlGCI \beta_{\leq p^+}
                    }{
                        p \in \val{\zadeh}{\ontology} \setminus \set{1},
                        \beta \in \conceptNames(\ontology)
                    }
                \end{align*}
        \end{subsolution}
    \end{solution}
\end{exercise}

\begin{exercise}
    \hfill
    \begin{definition}{def:complex-role-inclusion}
        A \emph{complex role inclusion} is of the form $r \composition s \dlGRI t$,
        where $r, s, t \in \roleNames$, and is satisfied by
        \begin{itemize}
            \item a classical interpretation $\I$ if $r^\I \composition s^\I \subseteq t^\I$,
                where $\composition$ denotes the composition of binary relations;
            \item a fuzzy interpretation $\I$ if
                \[
                    \forall d, f \in \domain{\I} \holds
                        \sup_{e \in \domain{\I}}
                            \left(
                                r^\I(d, e) \tnorm s^\I(e, f)
                            \right)
                        \leq t^\I(d, f)
                        .
                \]
        \end{itemize}
        $\ALCname$ extended with complex role inclusions is called $\ALCRname$.
    \end{definition}

    Extend the reduction
    $\zadeh-\ALC{=} \to \ALCname$
    to $\zadeh-\ALCR{=} \to \ALCRname$
    by defining $\kappa(r \composition s \dlGRI t)$
    and extending the proof of \missingReference{Lemma 15}
    to complex role inclusions.

    \begin{solution}
        Let
        \begin{align*}
            \ggeqSet{\ontology}
            &\definedas
                \set{\geq, >} \times \val{\zadeh}{\ontology} \\
            \ggeqSetReduced{\ontology}
            &\definedas
                \ggeqSet{\ontology} \setminus \set{\geq 0, > 1}
            .
        \end{align*}
        We extend the definition of $\kappa$ as follows:
        \[
            \kappa(r \composition s \dlGRI t)
            \definedas
            \setCondition{
                r_{\ggeq p}
                \composition
                s_{\ggeq p}
                \dlGRI
                t_{\ggeq p}
            }{
                \ggeq p \in \ggeqSetReduced{\ontology}
            }
        \]
%
        \begin{itemize}
            \item[$\Leftarrow$:]% Soundness
                Let $\I$ be an arbitrary (crisp) model of $\delta(\ontology) \union \kappa(\ontology)$
                and $\J$ the corresponding fuzzy interpretation
                \emph{(as defined in \missingReference{L02s60})}.
                \begin{observation}
                    For every $r \in \roleNames$ the sets
                    $
                        \tuple{(r_{\ggeq p})^\I}_{
                            \ggeq p \in \ggeqSetReduced{\ontology}
                        }
                    $
                    form a chain \wrt $\subseteq$.
                \end{observation}
                \begin{observation}
                    For $x \definedas r^\J(d, e)$:
                    \[
                        \forall p \in \ggeqSetReduced{\ontology} \holds
                            \begin{array}[t]{@{} l @{\ } l @{}}
                                &
                                \left(
                                    p \leq x
                                    \implication
                                    (d, e) \in (r_{\geq p})^\I
                                \right)
                                \\
                                \land
                                &
                                \left(
                                    p < x
                                    \implication
                                    (d, e) \in (r_{> p})^\I
                                \right)
                            \end{array}
                            .
                    \]
                \end{observation}
                \begin{claim}[Soundness of the reduction]
                    For every
                    $
                        r \composition s \dlGRI t
                        \in \ontology
                    $,
                    we have
                    \[
                        \J \models r \composition s \dlGRI t
                        .
                    \]
                %
                    \begin{proof}
                        For
                        $
                            r \composition s \dlGRI t
                            \in \ontology
                        $,
                        consider $d, e, f \in \domain{\J}$ and
                        \begin{align*}
                            x &\definedas r^\J(d, e) \\
                            y &\definedas s^\J(e, f) \\
                            m &\definedas x \tnorm y = \min\set{x, y} \\
                            z &\definedas t^\J(d, f)
                            .
                        \end{align*}
                        We show $m \leq z$.
                        \begin{caseDistinction}
                            \item If $m \in \val{\zadeh}{\ontology}$:
                                Using the \missingReference{observation above}
                                and $m \leq x$, $m \leq y$,
                                we get
                                \begin{align*}
                                    (d, e) &\in (r_{\geq m})^\I \\
                                    (e, f) &\in (s_{\geq m})^\I
                                    .
                                \end{align*}
                                Since $\I \models \kappa(r \composition s \dlGRI t)$,
                                we have
                                $
                                    (r_{\geq m})^\I \composition (s_{\geq m})^\I
                                    \subseteq
                                    (t_{\geq m})^\I
                                $
                                and thus,
                                \begin{align*}
                                    &(d, f) \in (t_{\geq m})^\I \\
                                    \ \therefore\ &z = t^\J(d, f) \geq m
                                    .
                                \end{align*}
                            \item
                                If $m = \frac{p + p^+}{2}$ for some $p \in \val{\zadeh}{\ontology}$:
                                Using the \missingReference{observation above}
                                and $p < m \leq x$, $p < m \leq y$,
                                we get
                                \begin{align*}
                                    (d, e) &\in (r_{> p})^\I \\
                                    (e, f) &\in (s_{> p})^\I
                                    .
                                \end{align*}
                                Since $\I \models \kappa(r \composition s \dlGRI t)$,
                                we have
                                $
                                    (r_{> p})^\I \composition (s_{> p})^\I
                                    \subseteq
                                    (t_{> p})^\I
                                $
                                and thus,
                                \begin{align*}
                                    &(d, f) \in (t_{> p})^\I \\
                                    \ \therefore\ &z = t^\J(d, f) > p \\
                                    \ \therefore\ &z \geq \frac{p + p^+}{2} = m
                                    .
                                \end{align*}
                        \end{caseDistinction}
                        With this, we can show the claim:
                        \begin{align*}
                            &\forall d, e, f \in \domain{\I} \holds
                                r^\I(d, e) \tnorm s^\I(e, f)
                                \leq t^\I(d, f) \\
                            \iff &\forall d, f \in \domain{\I} \holds
                                \sup_{e \in \domain{\I}}
                                    \left(
                                        r^\I(d, e) \tnorm s^\I(e, f)
                                    \right)
                                \leq t^\I(d, f) \\
                            \iff &\J \models r \composition s \dlGRI t
                        \end{align*}
                    \end{proof}
                \end{claim}
            \item[$\Rightarrow$:]% Completeness
                Let $\I$ be an arbitrary (fuzzy) model of the given ontology $\ontology$,
                and $\J$ the corresponding crisp interpretation
                \emph{(as defined in \missingReference{L02s61})}.
                \begin{claim}[Completeness of the reduction]
                    For every
                    $
                        r \composition s \dlGRI t
                        \in \ontology
                    $,
                    we have
                    \begin{align*}
                        &\J \models
                            \kappa(r \composition s \dlGRI t) \\
                        \text{\ie} &\forall \ggeq p \in \ggeqSetReduced{\ontology} \holds
                            \J \models
                            r_{\ggeq p} \composition s_{\ggeq p}
                            \dlGRI
                            t_{\ggeq p}
                        .
                    \end{align*}
                %
                    \begin{proof}
                        Consider
                        $
                            r \composition s \dlGRI t
                            \in \ontology
                        $
                        and any $\ggeq p \in \ggeqSetReduced{\ontology}$.
                        \begin{align*}
                            &\I \models r \composition s \dlGCI t \\
                            \iff &\forall d, f \in \domain{\I} \holds
                                \sup_{e \in \domain{\I}}
                                \left(
                                    r^\I(d, e) \tnorm s^\I(e, f)
                                \right)
                                \leq t^\I(d, f)
                                \transformationHint{%
                                    \substack{%
                                        \text{semantics of fuzzy} \\
                                        \text{$\composition$ and $\dlGRI$}
                                    }
                                } \\
                            \iff &\forall d, e, f \in \domain{\I} \holds
                                r^\I(d, e) \tnorm s^\I(e, f)
                                \leq t^\I(d, f) \\
                            \iff &\forall d, e, f \in \domain{\I} \holds
                                \min\set{r^\I(d, e), s^\I(e, f)}
                                \leq t^\I(d, f)
                                \transformationHintText{%
                                    definition of $\tnorm$
                                } \\
                            \iff &\forall d, e, f \in \domain{\I} \holds
                                r^\I(d, e) \leq t^\I(d, f)
                                \lor
                                s^\I(e, f) \leq t^\I(d, f) \\
                            \ \therefore\ & \forall d, e, f \in \domain{\J} \holds
                                \begin{array}[t]{@{} l @{\ } l @{}}
                                    &
                                    \left(
                                        (d, e) \in r^\J_{\ggeq p}
                                        \implication
                                        (d, f) \in t^\J_{\ggeq p}
                                    \right)
                                    \\
                                    \lor
                                    &
                                    \left(
                                        (e, f) \in s^\J_{\ggeq p}
                                        \implication
                                        (d, f) \in t^\J_{\ggeq p}
                                    \right)
                                \end{array}
                                \transformationHintText{%
                                    definition of $(r'_{\ggeq p})^\J$
                                } \\
                            \iff &\forall d, e, f \in \domain{\J} \holds
                                \left(
                                    (d, e) \in r^\J_{\ggeq p}
                                    \land
                                    (e, f) \in s^\J_{\ggeq p}
                                \right)
                                \implication
                                t^\J_{\ggeq p}(e, f) \\
                            \iff &\forall d, e, f \in \domain{\J} \holds
                                \left(
                                    r^\J_{\ggeq p}
                                    \composition
                                    s^\J_{\ggeq p}
                                \right)(d, f)
                                \implication
                                t^\J_{\ggeq p}(e, f) \\
                            \iff &r^\J_{\ggeq p}
                                \composition s^\J_{\ggeq p}
                                \subseteq
                                t^\J_{\ggeq p}
                                \transformationHintText{%
                                    semantics of crisp $\composition$
                                } \\
                            \iff &\J \models
                                r_{\ggeq p} \composition s_{\ggeq p}
                                \dlGRI
                                t_{\ggeq p}
                                \transformationHintText{%
                                    semantics of crisp $\dlGRI$
                                }
                        \end{align*}
                    \end{proof}
                \end{claim}
        \end{itemize}
    \end{solution}
\end{exercise}

\begin{exercise}[Optional]\label{ex:4.3}
    Continue \ref{ex:4.1}:
    \begin{subexercise}
        \item\label{ex:4.3:a}
            Define
            $\gamma(\dlExists{r}{D}, \leq p)$
            and $\gamma(\dlForall{r}{D}, \leq p)$.
    \end{subexercise}
    Extend the reduction to full ABoxes (not just local ABoxes) by using also role names of the form $r_{\leq p}$:
    \begin{subexercise}[resume]
        \item\label{ex:4.3:b}
            Define $\kappa(\fuzzyRoleAssertion{a}{b}{r}{= p})$.
        \item\label{ex:4.3:c}
            What additional expressivity is needed
            to encode the behavior of $r_{\leq p}$
            in $\delta(\ontology)$?
    \end{subexercise}
%
    \begin{solution}
        \begin{subsolution}
            \item[\ref{ex:4.3:a}]
                \missingText{Solution}
            \item[\ref{ex:4.3:b}]
                \missingText{Solution}
            \item[\ref{ex:4.3:c}]
                \missingText{Solution}
        \end{subsolution}
    \end{solution}
\end{exercise}

\begin{exercise}[Optional]\label{ex:4:4}
    Adapt the proof of \missingReference{Lemma~15} to show the following claim:
    \begin{claim}\hypertarget{ex:4.4:claim}{}
        If a $\zadeh-\ALC{=}$ TBox $\TBox$ has a model $\I$
        with $A^\I(d) > p$ for some $p \in \val{\zadeh}{\ontology}$,
        then it also has a model $\J$
        with $A^\J(d) > p^+ - \varepsilon$,
        for all values $\varepsilon \in \left(0, p^+\right]$.
    \end{claim}
    Use this insight to develop an exponential-time algorithm
    to compute the best satisfiability degree
    of a concept $C$ \wrt a $\zadeh-\ALC{=}$ TBox $\TBox$.
%
    \begin{solution}
        \begin{proof}[(\hyperlink{ex:4.4:claim}{Claim})]
            \missingText{Proof}
        \end{proof}
        \missingText{Algorithm}
    \end{solution}
\end{exercise}
