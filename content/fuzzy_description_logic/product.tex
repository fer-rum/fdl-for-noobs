\section{The Crisp Model Property}

\begin{lemma}
    Lemma 21\hfill
    For any $x, y \in \fuzzyDomain$, we have
    $x \residuum y = 0 \iff x > 0 \land y = 0$.
%
    \begin{proof}
        \begin{itemize}
            \item[$\Rightarrow$:]
                If $x > 0$ and $y = 0$, then by \missingReference{Lemma 3},
                \begin{align*}
                    x \residuum y
                    &= x \residuum 0\\
                    &= \max\setCondition{z}{x \tnorm z \leq 0}\\
                    &= 0,
                \end{align*}
                since $\tnorm$ has no nilpotent elements.
            \item[$\Leftarrow$:]
                \begin{caseDistinction}
                    \item If $x = 0$,
                        then $x \residuum y = 0 \residuum y = 1 > 0$ by \missingReference{Lemma 2}.
                    \item If $y > 0$,
                        then $x \residuum y = \max\setCondition{z}{x \tnorm z \leq y} \geq y > 0$,
                        since $x \tnorm y \leq 1 \tnorm y = y$ by the properties of $\tnorm$.
                    \qedhere
                \end{caseDistinction}
        \end{itemize}
    \end{proof}
\end{lemma}

\begin{lemma}
    Lemma 22
%
    \begin{proof}
        Observe that $\crisp{x} = \precomplement\precomplement(x)$ for all $x \in \fuzzyDomain$.
        \begin{properties}
            \item $\crisp{\precomplement(x)} = \precomplement\precomplement\precomplement(x) = \precomplement\crisp{x}$.
            \item Since $\tnorm$ has no nilpotent elements, we have
                $x \tnorm y = 0 \iff x = 0 \lor y = 0$, and hence
                \begin{align*}
                    \crisp{x \tnorm y} = 0
                    &\iff x \tnorm y = 0\\
                    &\iff x = 0 \lor y = 0\\
                    &\iff \crisp{x} = 0 \lor \crisp{y} = 0\\
                    &\iff \crisp{x} \tnorm \crisp{y} = 0.
                \end{align*}
                Since both $\crisp{x \tnorm y}$ and $\crisp{x} \tnorm \crisp{y}$ are
                either 0 or 1, this shows that $\crisp{x \tnorm y} = \crisp{x} \tnorm \crisp{y}$.
            \item Since 0 is the unit \wrt $\tconorm$, we have
                $x \tconorm y = 0 \iff x = y = 0$, and thus
                \begin{align*}
                    \crisp{x \tconorm y} = 0
                    &\iff x \tconorm y = 0\\
                    &\iff x = y = 0\\
                    &\iff \crisp{x} = \crisp{y} = 0\\
                    &\iff \crisp{x} \tconorm \crisp{y} = 0.
                \end{align*}
            \item By \missingReference{Lemma 21}, we have
                \begin{align*}
                    \crisp{x \residuum y} = 0
                    &\iff x \residuum y = 0\\
                    &\iff x > 0 \text{ and } y = 0\\
                    &\iff \crisp{x} = 1 \text{ and } \crisp{y} = 0\\
                    &\iff \crisp{x} \residuum \crisp{y} = 0.
                \end{align*}
            \item
                \begin{align*}
                    \crisp{\max\setCondition{x}{x \in X}} = 0
                    &\iff \max\setCondition{x}{x \in X} = 0\\
                    &\iff X = \set{0}\\
                    &\iff \crisp{x} = 0 \text{ for } x \in X\\
                    &\iff \max\setCondition{\crisp{x}}{x \in X} = 0.
                \end{align*}
            \item Let $x_0 \in X$ be such that $x_0 = \min\setCondition{x}{x \in X}$.
                Then,
                \begin{align*}
                    \crisp{\min\setCondition{x}{x \in X}} = 0
                    &\iff x_0 = 0\\
                    &\iff \crisp{x_0} = 0\\
                    &\iff \min\setCondition{\crisp{x}}{x \in X} = 0.
                \end{align*}
            \qedhere
        \end{properties}
    \end{proof}
\end{lemma}

\lecturedate{2019-01-09}

\begin{definition}{}
    $X-\ALC{\greaterOrGreaterEqual}$ has the \emph{crisp model property} if every locally consistent ontology has a model that uses only the truth values $0$ and $1$.
\end{definition}

\begin{lemma}
    Lemma 23
%
    For $X \in \set{\product, \goedel}$, $X-\ALC{\geq}$ has the crisp model property.
%
    \begin{proof}
        Let $\ontology$ be an $X-\ALC{\geq}$ ontology and $\I \models \ontology$.
        We define a crisp model $\J$ of $\ontology$ as follows:
        \begin{align*}
            \domain{\J} &\definedas \domain{\I}\\
            a^\J &\definedas a^\I\\
            A^\J(d) &\definedas \crisp{A^\I(d)}\\
            r^\J(d, e) &\definedas \crisp{r^\I(d, e)}
        \end{align*}
        \begin{claim}
            For all $d \in \domain{\I}$ and $C \in \subconcepts{\ontology}$, we have $C^\J(d) = \crisp{C^\I(d)}$.
            \begin{proof}
                We prove the claim by induction on the structure of $C$.
                \begin{caseDistinction}
                    \item
                        Cases
                        $C = A \in \conceptNames$,
                        $C = \top$,
                        $C = \bottom$:
                        by definition
                    \item Case $C = D \dlAnd E$:
                        \begin{align*}
                            (D \dlAnd E)^\J(d)
                            &= D^\J(d) \tnorm E^\J(d)
                            \\
                            &= \crisp{D^\J(d)} \tnorm \crisp{E^\J(d)}
                            &&\vert\text{ IH}
                            \\
                            &= \crisp{D^\J(d) \tnorm E^\J(d)}
                            &&\vert\text{ \missingReference{Lemma 22}}
                            \\
                            &= \crisp{(D \dlAnd E)^\J(d)}
                        \end{align*}
                    \item
                        Cases
                        $C = D \dlOr E$,
                        $C = D \dlImplication E$,
                        $C = \dlNot D$:
                        similar
                    \item Case $C = \dlExists{r}{D}$:
                        \begin{align*}
                            (\dlExists{r}{D})^\J(d)
                            &= \max_{e \in \domain{\J}}
                                r^\J(d, e) \tnorm D^\J(e)
                            \\
                            &= \max_{e \in \domain{\J}}
                                \crisp{r^\J(d, e)} \tnorm \crisp{D^\J(e)}
                            &&\vert\text{ IH}
                            \\
                            &= \crisp{
                                \max_{e \in \domain{\J}}
                                r^\J(d, e) \tnorm D^\J(e)
                            }
                            \\
                            &= \crisp{(\dlExists{r}{D})^\J(d)}
                        \end{align*}
                    \item Case $C = \dlForall{r}{D}$: similar
                    \qedhere
                \end{caseDistinction}
            \end{proof}
        \end{claim}
        This can be used to show that $\J$ is also a model of $\ontology$:
        \begin{itemize}
            \item Consider any $\fuzzyConceptAssertion{a}{C}{\geq p}$.
                \begin{caseDistinction}
                    \item If $p = 0$,
                        then $\J$ trivially satisfies this axiom.
                    \item If $p > 0$,
                        then $C^\J(a^\J) \geq p > 0$
                        and thus,
                        $C^\J(a^\J) = \crisp{C^\J(a^\J)} = 1 \geq p$.
                \end{caseDistinction}
            \item Consider any $C \dlGCI D \in \ontology$.
                \begin{caseDistinction}
                    \item If $C^\J(d) = 0$ for some $d \in \domain{\J}$,
                        then clearly $C^\J(d) \leq D^\J(d)$.
                    \item Otherwise,
                        we have $D^\J(d) \geq C^\J(d) > 0$
                        and thus,
                        $D^\J(d) = C^\J(d) = 1$.
                    \qedhere
                \end{caseDistinction}
        \end{itemize}
    \end{proof}
\end{lemma}

\begin{lemma}
    Lemma 24
%
    For $X \in \set{\product, \goedel}$, local consistency in $X-\ALC{\geq}$ is \ExpTime-complete.
%
    \begin{proof}
        \missingText{Proof on slide 77}
    \end{proof}
\end{lemma}

\section{The Post Correspondence Problem}

\subsection{Product Operators}
\missingText{slide 78}

\begin{definition}{}
    Let $\PCPset = \set{(v_1, w_1), \dots, (v_k, w_k)}$
    be a finite set of pairs of words over the alphabet $\PCPalphabet$.
    \newline
    The Post correspondence problem asks whether there is a finite non-empty sequence
    $i_1 \dots i_n \in K^+$ (where $K \definedas \set{1, \dots, k}$)
    such that $v_{i_1} \dots v_{i_n} = w_{i_1} \dots w_{i_n}$.
    \newline
    If this sequence exists, it is called a solution for $\PCPalphabet$.
\end{definition}

\begin{remark}
    \begin{itemize}
        \item
            This decision problem is undecidable (but semi-decidable) for any alphabet $\PCPalphabet$ with at least $2$ letters.
        \item
            For the following reduction, we choose
            $\PCPalphabet \definedas \set{1, \dots, 9}$.
        \item
            For $u = i_1 \dots i_n \in K^*$,
            we denote $v_u \definedas v_{i_1} \dots v_{i_n}$
            and $w_u \definedas w_{i_1} \dots w_{i_n}$.
        \item
            $u \in K^*$ are the nodes of the search tree of $\PCPset$.
    \end{itemize}
\end{remark}

\begin{example}[label = expl:fdl:pcp]
    \[
        \PCPset \definedas \set{
            (\underset{v_1}{1}, \underset{w_1}{12}),
            (\underset{v_2}{22}, \underset{w_2}{2})
        }
    \]
    \begin{align*}
        u   &= 1\ 2\\
        v_u &= 1 | 22\\
        w_u &= 12 | 2
    \end{align*}
\end{example}

\missingText{The Search Tree, slide 80}

\begin{example}[continues = expl:fdl:pcp]
    \missingText{Search Tree}
\end{example}

\missingText{Encoding the PCP, slide 81}

\missingText{The Canonical Model, slide 82}

\missingText{Representing the Search Tree, slide 83}

\missingText{Initialize}

\missingText{Concatenate}

\missingText{Successors}

\missingText{Transfer}

\begin{lemma}
    Lemma 27
%
    Let $C$, $D$ be concepts and $r$ a role name.
    For every model $\I$ of
    $\set{\dlExists{r}{D} \dlGCI C, C \dlGCI \dlForall{r}{D}}$
    and all $d, e \in \domain{\I}$ with $r^\I(d, e) = 1$,
    we have $C^\I(d) = D^\I(e)$.
%
    \begin{proof}
        Since $\I \models \dlExists{r}{D} \dlGCI C$,
        we have
        \begin{align*}
            D^\I(e)
            &= \underbrace{r^\I(d, e)}_{= 1} \tnorm D^\I(e)\\
            &\leq (\dlExists{r}{D})^\I(d)\\
            &\leq C^\I(d).
        \end{align*}
        Similarly $\I \models C \dlGCI \dlForall{r}{D}$
        yields
        \begin{align*}
            C^\I(d)
            &\leq (\dlForall{r}{D})^\I(d)\\
            &= \underbrace{r^\I(d, e)}_{= 1} \residuum D^\I(e)\\
            &= D^\I(e)
            &&\vert\text{ \autoref{thm:tnorm-residua}}
        \end{align*}
        Thus, we have $C^\I(d) = D^\I(e)$.
        \qedhere
    \end{proof}
\end{lemma}

\missingText{Definition of $\ontology^{\text{tra}}_\PCPset$}

\lecturedate{2019-01-16}

\subsection{Canonical Model Embedding}

\begin{remark}
    The canonical model can be embedded into every model of these axioms:
    \enquote{We can always find the tree inside.}
\end{remark}

\begin{lemma}
    Lemma 28
%
    For every model $\I$ of $\ontology^\text{ini}_\mathcal{P} \union \ontology^\text{con}_\mathcal{P} \union \ontology^\text{suc}_\mathcal{P} \union \ontology^\text{tra}_\mathcal{P}$, there is a mapping $g: \domain{\I_\mathcal{P}} \to \domain{\I}$, such that
    $
        A^\I(g(u)) = A^{\I_\mathcal{P}}(u)
    $
    holds for all $u \in K^*$ and all $A \in \setCondition{V, W, M, V_i, W_i, V_i^+, W_i^+}{i \in K}$, and
    $
        r^\I_i(g(u), g(ui)) = r^{\I_\mathcal{P}}_i(u, ui) = 1
    $
    holds for all $u \in K^*$ and all $i \in K$.
    %
    \begin{proof}
        We inductively construct $g$ and show the required properties for $V$, $V_i$ and $V_i^+$ for all $i \in K$.
        The arguments for $M$, $W$, $W_i$ and $W_i^+$ are similar.

        \begin{description}
            \item[Start:]
                Initially, we set $g(\varepsilon) \definedas a^\I$.
                Due to $\ontology_\mathcal{P}^\text{ini}$, we have
                $V^\I(g(\varepsilon)) = V^\I(a^\I) = 1 = \enc{v_\varepsilon} = V^{I_\mathcal{P}}(\varepsilon)$
                and $V_i^\I(g(\varepsilon)) = \enc{v_i} = V_i^{I_\mathcal{P}}(\varepsilon)$.
            \item[Step:]
                Assume now that we have already defined $g(u)$ for $u \in K^*$
                such that $V^\I(g(u)) = V^{I_\mathcal{P}}(u) = \enc{v_u}$ and $V_i^\I(g(u)) = V_i^{I_\mathcal{P}}(u) = \enc{v_i}$.

                Due to $\ontology^\text{con}_\mathcal{P}$ and \missingReference{Lemma 25}, we know that $(V_i^+)(g(u)) = \enc{V_{ui}} = (V_i^+)^{\I_\mathcal{P}}(u)$.
                Due to $\ontology^\text{suc}_\mathcal{P}$ and \missingReference{Lemma 26}, for each $j \in K$, there is an element $e_j \in \domain{\I}$ with $r_j^\I(g(u), e_j) = 1$.
                We set $g(uj) \definedas e_j$ for all $j \in K$.
                Due to $\ontology^\text{tra}_\mathcal{P}$ and \missingReference{Lemma 27}, we have $V^\I(g(uj)) = (V_j^+)(g(u)) = \enc{v_{uj}} = V^{\I_\mathcal{P}}(uj)$
                and $V_i^\I(g(uj)) = V_i^\I(g(u)) = \enc{v_i} = V_i^{\I_\mathcal{P}}(uj)$ for all $j \in K$.
                \qedhere
        \end{description}
    \end{proof}
\end{lemma}

\subsection{Solution}

\begin{remark}
    It remains to check $\I_\PCPset$ for a solution.
\end{remark}

\begin{lemma}
    Lemma 29
%
    For any interpretation $\I$, $d \in \domain{\I}$ and $u \in K^*$:
    If $V^\I(d) = \enc{v_u}$ and $W^\I(d) = \enc{w_u}$, then
    \[
        v_u \neq w_u
        \iff
        (V \dlImplication W)^\I(d) \leq 0.5
        \lor
        (W \dlImplication V)^\I(d) \leq 0.5.
    \]
%
    \begin{proof}
        Observe first that $(V \dlImplication W)^\I(d) = 1$ or $(W \dlImplication V)^\I(d) = 1$, because either $\enc{v_u} \leq \enc{w_u}$ or $\enc{w_u} < \enc{v_u}$.
        \begin{itemize}
            \item[$\Leftarrow$:]
                Let $v_u = w_u$.
                Then, $\enc{v_u} = \enc{w_u}$, and thus $(V \dlImplication W)^\I(d) = \enc{v_u} \residuum \enc{w_u} = 1 > 0.5$ and $(W \dlImplication V)^\I(d) = 1 > 0.5$.
            \item[$\Rightarrow$:]
                Let $v_u \neq w_u$.
                \begin{caseDistinction}
                    \item Let $v_u < w_u$ and in particular $v_u + 1 \leq w_u$.
                        Thus,
                        \[
                            \enc{w_u} = 2^{-w_u} \leq 2^{-(v_u + 1)} = \frac{2^{-v_u}}{2} = \frac{\enc{v_u}}{2},
                        \]
                        which implies that $(V \dlImplication W)^\I(d) = \enc{v_u} \residuum \enc{w_u} = \frac{\enc{w_u}}{\enc{v_u}} \leq 0.5$.
                    \item Let $v_u > w_u$ and in particular $w_u + 1 \leq v_u$.
                        Then, similarly, $(W \dlImplication V)^\I(d) \leq 0.5$.
                \end{caseDistinction}
        \end{itemize}
    \end{proof}
\end{lemma}

\begin{remark}
    We check that for all $u \in K^* \setminus \set{\varepsilon}$ the words $v_u$ and $w_u$ are different:
    \[
        \ontology_\PCPset^\text{sol}
        \definedas
        \setCondition{
            \top \dlGCI
            \dlForall{r_i}{
                \left(
                    ((V \dlImplication W) \dlAnd (W \dlImplication V))
                    \dlImplication M
                \right)
            }
        }{i \in K}
    \]
\end{remark}

\begin{lemma}
    Lemma 30

    $\PCPset$ has a solution iff
    $
        \ontology_\PCPset
        \definedas
            \ontology_\PCPset^\text{ini}
            \union \ontology_\PCPset^\text{con}
            \union \ontology_\PCPset^\text{suc}
            \union \ontology_\PCPset^\text{tra}
            \union \ontology_\PCPset^\text{sol}
    $
    is \emph{not} locally consistent.
    %
    \begin{proof}
        \begin{itemize}
            \item[$\Leftarrow$:]
                Let $\PCPset$ have a solution, \ie there are $u \in K^*$, $i \in K$, such that $v_{ui} = w_{ui}$.
                Then, $(V \dlImplication W)^\I(g(ui)) = 1$ and $(W \dlImplication V)^\I(g(ui)) = 1$,
                for all models $\I$ of $\ontology_\PCPset$,
                where $g : \domain{\I_\PCPset} \to \domain{\I}$ is the mapping that exists by \missingReference{Lemma 28}.
                Thus,
                \[
                    \left(
                        ((V \dlImplication W) \dlAnd (W \dlImplication V))
                        \dlImplication M
                    \right)^\I(g(ui))
                    = M^\I(g(ui))
                    = 0.5
                \]
                (by \missingReference{Lemma 2}), and
                \begin{align*}
                    1 = \top^\I(g(u))
                    & \leq (\dlForall{r_i}{((V \dlImplication W) \dlAnd (W \dlImplication V)) \dlImplication M})^\I(g(u)) \\
                    & \leq \underbrace{r_i^\I(g(u), g(ui))}_{= 1} \residuum \underbrace{(((V \dlImplication W) \dlAnd (W \dlImplication V)) \dlImplication M)^\I(g(ui))}_{= 0.5} \\
                    & = 0.5 \quad \lightning
                \end{align*}
                Thus, $\ontology_\PCPset$ can not have any models.
            \item[$\Rightarrow$:]
                Let $\ontology_\PCPset$ not have any model.
                Then, the canonical model $\I_\PCPset$, which is clearly a model of
                $
                    \ontology_\PCPset^\text{ini}
                    \union \ontology_\PCPset^\text{con}
                    \union \ontology_\PCPset^\text{suc}
                    \union \ontology_\PCPset^\text{tra}
                $,
                must violate an axiom
                \[
                    \top \dlGCI
                    \dlForall{r_i}{
                        \left(
                            ((V \dlImplication W) \dlAnd (W \dlImplication V))
                            \dlImplication M
                        \right)
                    },
                \]
                for some $i \in K$.
                Thus, there is at least one $u \in K^*$ for which
                \[
                    \top \dlGCI
                    \dlForall{r_i}{
                        \left(
                            ((V \dlImplication W) \dlAnd (W \dlImplication V))
                            \dlImplication M
                        \right)
                    }
                    < 1.
                \]
                In particular,
                \[
                    \underbrace{r^{\I_\PCPset}(u, ui)}_{= 1} \residuum \underbrace{(((V \dlImplication W) \dlAnd (W \dlImplication V)) \dlImplication M)^{\I_\PCPset}(ui)}_{< 1}
                    < 1,
                \]
                and thus
                \[
                    ((V \dlImplication W) \dlAnd (W \dlImplication V))^{\I_\PCPset}(ui)
                    > M^{\I_\PCPset}(ui)
                    = 0.5.
                \]
                This means that
                    \begin{align*}
                        0.5
                        &< ((V \dlImplication W) \dlAnd (W \dlImplication V))^{\I_\PCPset}(ui) \\
                        &\leq \min\set{(V \dlImplication W)^{\I_\PCPset}(ui), (W \dlImplication V)^{\I_\PCPset}(ui)} \\
                        &\leq (V \dlImplication W)^{\I_\PCPset}(ui), \quad \text{and} \\
                        0.5
                        &< (W \dlImplication V)^{\I_\PCPset}(ui).
                    \end{align*}
                By \missingReference{Lemma 29}, we have that $v_{ui} = w_{ui}$, i.e., $\PCPset$ has a solution.
                \qedhere
        \end{itemize}
    \end{proof}
\end{lemma}

\lecturedate{2019-01-18}

\subsection{Example of the Complete Reduction}

\begin{example}
    \missingText{Definition, slide 90}
    \missingText{Search-Tree}
\end{example}
