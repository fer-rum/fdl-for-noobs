\section{The Gödel T-Norm}

\begin{definition}[Gödel T-norm]{def-tnormG}
    $x \tnormG y  = \min\set{ x, y }$
\end{definition}

\begin{figure}
    \centering
        \includegraphics[width=\linewidth]{images/tnormg}
    \caption{Graphical Representation of the Gödel-T-Norm}
\end{figure}

\begin{statement}
    $\tnormG$ fulfills all properties of $\tnorm$.
    \begin{proof}
    To show that $\tnormG$ is a t-norm, it has to be shown 
    that all properties of a t-norm are also properties of $\tnormG$.

        \begin{subproof}[Commutativity]
            The commutativity of $\tnormG$ follows directly 
            from the commutativity of the minimum-operator 
            (c.f. Statement \ref{stmt-min-commutative})
        \end{subproof}

        \begin{subproof}[Associativity]
            The associativity of $\tnormG$ follows directly 
            from the associativity of the minimum-operator 
            (c.f. Statement \ref{stmt-min-associative})
        \end{subproof}

        \begin{subproof}[Monotonicity] 
            % TODO it would be more useful to proof the monotonicity of \min 
            % and then refer to it here
            For a fixed $y \in \fuzzyDomain$ let there be $x, x' \in \fuzzyDomain$ 
            with $x' \geq x$.\\
            To show that then $x' \tnormG y \geq x \tnormG y$, the following 
            condition has to be fulfilled:
            \begin{equation*}
                \min\set{ x, y} \leq \min\set{x', y}
            \end{equation*}

            This can be done by a case distinction.

            \begin{itemize}
            \item If $x' \leq y$, both minimums yield $y$ 
                and the required condition holds.\\
            \item If $x' > y$ and $x \leq y$, the evaluation of the condition 
                yields $y < x'$, which is exactly the assumption for this case.\\
            \item If $x' > y$ and $x > y$, the evaluation of the condition yields 
                $x < x'$, which is contained by the initial definition 
                of $x'$.\\
            \end{itemize}

            Since any t-norm is commutative 
                (c.f. definition \ref{def-tnorm-commutative}), 
            the argument holds as well for a fixed $x$ 
                and any $y' \geq y \in \fuzzyDomain$. 
        \end{subproof}

        \begin{subproof}[Unit element is 1]
            Since $x \in \fuzzyDomain$, $x \ngtr 1$.
            Therefore, $x \tnormG 1 = \min\set{ x, 1 } = x$
        \end{subproof}
    \end{proof}
\end{statement}

\subsection{Derived Operators}

\begin{definition}[Residuum]{def-tnormg-residuum}
    \begin{align*}
        \residuumG:\fuzzyDomain \times \fuzzyDomain \to \fuzzyDomain&\\
        x \residuumG y &= 
            \begin{cases}
                y & \text{if} \quad x > y\\
                1 & \text{otherwise}
            \end{cases}
    \end{align*}
\end{definition}

\begin{statement}\label{stmt-tnormg-residuum-pp1}
    From definition \ref{def-tnormg-residuum} directly follows that 
    $p \residuumG p = 1$.
\end{statement}

\begin{definition}[Precomplement]{def-tnormg-precomplement}
    \begin{align*}
        \precomplementG:\fuzzyDomain \to \fuzzyDomain&\\
        \precomplementG x &= 
            \begin{cases}
                1 & \text{if} \  x = 0\\
                0 & \text{otherwise}
            \end{cases}
    \end{align*}
\end{definition}

\begin{definition}[Co-norm]{def-tnormg-conorm}
    \begin{align*}
        \tconormG:\fuzzyDomain \times \fuzzyDomain \to \fuzzyDomain&\\
        x \tconormG y &= \max\set{x, y}
    \end{align*}
\end{definition}

\subsection{Properties}

\begin{statement}
    $\tnormG$ is continuous since $(x, y) \mapsto \min\set{x, y}$ is continuous.
\end{statement}

\begin{statement}
    Each value of $x \in \fuzzyDomain$ is idempotent with respect to $\tnormG$ 
    (c.f. definition \ref{def-tnorm-idempotent}).
    
    \begin{proof}
        $x \tnormG x = \min\set{x, x} = x$
    \end{proof}

    % TODO graph idempotent elements
    
\end{statement}

\begin{statement}
    There are no nilpotent elements with respect to $\tnormG$
    (c.f. definition \ref{def-tnorm-nilpotent}).
    
    \begin{proof}
        Assume there to be $x, y \in \fuzzyDomain \setminus \set{0}$ and $x \leq y$.\\
        Then $x \tnormG y = y \tnormG x = \min\set{x, y} = x$.\\
        Since $x \neq 0$ was assumed, $\nexists x: x \tnormG y = 0$.
    \end{proof}

\end{statement}

\subsection{G-Tautologies}

\begin{statement}
    $\models_G \left( p \wedge q \right) \rightarrow p$
    \begin{proof}
        \begin{align*}
            \valuation{G}{ \left( p \wedge q \right) \rightarrow p} 
            &= \min\set{p, p} \residuumG p
        \end{align*}
    \begin{itemize}
        \item If $q < p$:
            \begin{align*}
                \min\set{p, q} &= q 
                    &&\sidenote{by case assumption}\\
                q \residuumG p &= 1 
                    &&\sidenote{by definition \ref{def-tnormg-residuum}}
            \end{align*}
        \item If $q \geq p$:
        \begin{align*}
                \min\set{p, q} &= p &&\sidenote{by case assumption}\\
                p \residuumG p &= 1 &&\sidenote{by statement 
                    \ref{stmt-tnormg-residuum-pp1}}
            \end{align*}
    \end{itemize}
    Since the valuations of all cases are \emph{G-models}, 
    the propositional formula is a \emph{G-tautology}.
    \end{proof}
\end{statement}

\begin{statement}
    $\nvDash_G p \vee \neg p$
    \begin{proof}

        \begin{align*}
            \valuation{G}{ p \vee \neg p }
            &= \max\set{p, \precomplementG p}
        \end{align*}

        When $0 < p < 1$, $\max\set{p, 0} = p$.
        Since in this case $p \neq 1$, the formula is no \emph{G-tautology}.
    \end{proof}
\end{statement}

\begin{statement}
    $\models_G p \rightarrow \left( \neg \left( \neg p \right) \right) $

    \begin{proof}
        \begin{align*}
            \valuation{G}{ p \rightarrow \left( 
                \neg \left( \neg p \right) \right) 
            } &= p \residuumG \precomplementG \left( \precomplementG p \right)
        \end{align*}
        \begin{itemize}
            \item If $p = 0$:
            \begin{align*}
                & 0 \residuumG \precomplementG 1 \\
                &= 0 \residuumG 0 \\
                &= 1 &&\sidenote{c.f. statement \ref{stmt-tnormg-residuum-pp1}}
            \end{align*}

            \item If $p \neq 0$:
            \begin{align*}
                & p \residuumG \precomplementG 0 \\
                &= p \residuumG 1 \\
                &= 1 &&\sidenote{c.f. lemma \ref{lemma-tnorm-residua}}
            \end{align*}
        \end{itemize}
    Since the valuations of all cases are \emph{G-models}, 
    the propositional formula is a \emph{G-tautology}.
    \end{proof}

\end{statement}
