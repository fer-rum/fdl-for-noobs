\section{General Triangular Norms}

\begin{definition}{def-tnorm}

    A \textbf{triangular norm} is a binary operator:
    \begin{equation*}
    \tnorm: \fuzzyDomain \times \fuzzyDomain \to \fuzzyDomain
    \end{equation*}

    \begin{subdefinition}
        Each t-norm is \textbf{Associative}:
        \begin{equation*}
        x \tnorm \left ( y \tnorm z \right ) = \left( x \tnorm y \right) \tnorm z
        \end{equation*}
    \end{subdefinition}

    \begin{subdefinition}\label{def-tnorm-commutative}
        Each t-norm is \textbf{Commutative}:
        \begin{equation*}
        x \tnorm y = y \tnorm x
        \end{equation*}
    \end{subdefinition}

    \begin{subdefinition}\label{def-tnorm-monotone}
        Each t-norm is \textbf{Monotone} in both arguments:
        \begin{align*}
        \forall x, x', y, y' \in \fuzzyDomain.\,
            x \leq x'.\,
            y \leq y' :\\
            x \tnorm y \leq x' \tnorm y'
        \end{align*}
    \end{subdefinition}

    \begin{subdefinition}\label{def-tnorm-unit}
        Each t-norm has 1 as the \textbf{Unit element}:
        \begin{equation*}
        1 \tnorm x = x
        \end{equation*}
    \end{subdefinition}

\end{definition}

\begin{definition}{def-tnorm-idempotent}
    An element $x \in \fuzzyDomain$ is called \textbf{idempotent} 
        with respect to $\tnorm$ if 
    $x \tnorm x = x$.
\end{definition}

\begin{definition}{def-tnorm-nilpotent}
    An element $x \in \fuzzyDomain \setminus \set{0}$ is called \textbf{nilpotent} 
        with respect to $\tnorm$ if 
    $\exists x, y \in \fuzzyDomain \setminus \set{0}:\, x \tnorm y = 0$.
\end{definition}

\begin{statement}
    There are uncountably many t-norms.
    % TODO source or proof?
\end{statement}

\begin{statement}
    Every t-norm is compatible with the classical conjunction 
    on $\left\lbrace 0, 1 \right\rbrace$.

    \begin{proof}
        The cases
        \begin{align*}
            0 \tnorm 1 &= 0\\
            1 \tnorm 0 &= 0\\
            1 \tnorm 1 &= 1\\
        \end{align*}
        follow directly from definition \ref{def-tnorm-unit}
        of the t-norm's unit element.\\
        For the remaining case
        \begin{align*}
            0 \tnorm 0 &= 0\\
        \end{align*}
        let there be an $x \leq 1$.
        From definition \ref{def-tnorm-monotone} (monotonicity of t-norms) 
        follows that 
        \begin{align*}
            x \tnorm 0 &\leq 1 \tnorm 0\\
            x \tnorm 0 &\leq 0\\
        \end{align*}
        Since $x \tnorm 0 < 0$ is not possible in the domain, 
        $x \tnorm 0 = 0$ follows for $x < 1$ in general and $x = 0$ especially.
    \end{proof}

\end{statement}

\begin{definition}[Continuity]{def-tnorm-continuity}
    A t-norm $\tnorm$ is continuous if for all convergent sequences 
    $(x_n)_{n \in \N}, (y_n)_{n \in \N} \in \fuzzyDomain^\N$, it holds that

    \begin{align*}
        \left( \lim_{n \rightarrow \infty} x_n \right)
            \tnorm
        \left( \lim_{n \rightarrow \infty} y_n \right) 
        &= \lim_{n \rightarrow \infty} \left( x_n \tnorm y_n \right)
    \end{align*}

\end{definition}

\begin{lemma}\label{lemma-tnorm-continuous}
    Every continuous t-norm preserves suprema and infima, i.e.
    \begin{align*}
        \forall n \in \N.&\\
        \forall x, y_n \in \fuzzyDomain:&\\
        x \tnorm \text{sup}\set{ y_n } &= \text{sup}\set{ x \tnorm y_n }\\
        x \tnorm \text{inf}\set{ y_n } &= \text{inf}\set{ x \tnorm y_n }
    \end{align*}

    \begin{proof}
        Since $\tnorm$ is monotone by definition \ref{def-tnorm-monotone},
        \begin{align*}
            \forall m, n \in \N.&\\
            \forall x, y_m, y_n \in \fuzzyDomain.\,&\\ y_n \geq y_m:&\\
            x \tnorm y_n \geq x \tnorm y_m
        \end{align*}
        has to hold.
        It can be assumed \emph{w.l.o.g} that $y_{n+1} \geq y_n$.
        Then
        \begin{align*}
            \lim_{n \rightarrow \infty}(y_n) &= \text{sup}\set{ y_n } \\
            \lim_{n \rightarrow \infty}(x \tnorm y_n) &= \text{sup}\set{ x \tnorm y_n }
        \end{align*}
        % NOTE: I switched the index n in the following part for an i 
        % compared to the original proof to avoid confusion with the n that was
        % alredy used above.
        If we choose $(x_i)_{i \in \N}$ 
        such that $\forall i: x_i = x$, 
        the claim follows from definition \ref{def-tnorm-continuity}
        (continuity of $\tnorm$):
        \begin{align*}
			 \text{sup}\set{ x \tnorm y_n } 
			 &= \lim_{i \rightarrow \infty}(x \tnorm y_n)\\
			 &= \lim_{i \rightarrow \infty}(x_i \tnorm y_n)\\
			 &= \left( \lim_{i \rightarrow \infty} x_i \right)
                \tnorm
                \left( \lim_{i \rightarrow \infty} y_n \right)\\
			 &= x \tnorm \left( \lim_{i \rightarrow \infty}(y_n) \right)\\
			 &= x \tnorm \text{sup}\set{ y_n }
		\end{align*}
    \end{proof}
\end{lemma}

\begin{definition}[Residuum]{def-tnorm-residuum}
    The \textbf{residuum} $\residuumX$ of a t-norm $\tnormX$ 
    is a binary operator
    \begin{equation*}
        \residuumX: \fuzzyDomain \times \fuzzyDomain \to \fuzzyDomain
    \end{equation*}

    for which holds $\forall x, y, z \in \fuzzyDomain:$

    \begin{equation*}
        z \leq \left( x \residuumX y \right)
        \iff \left(x \tnormX z \right) \leq y
    \end{equation*}

\end{definition}

\begin{lemma}\label{thm:tnorm-residua}
    For $\residuum$ of any $\tnorm$ and $x, y \in \fuzzyDomain$ holds:
    \begin{align*}
        1 \residuum y &= y\\
        x \residuum 1 &= 1\\
        x \residuum y &= 1 \iff x \leq y
    \end{align*}

    \begin{proof}
        The proof is made by case distinction over $x$, $y$, $z$ of definition
        \ref{def-tnorm-residuum}:
        \begin{itemize}
            \item Let $x = 1$:
            \begin{align*}
                z \leq \left( 1 \residuum y \right) 
                    & \iff \left( 1 \residuum z \right) \leq y \\
                \therefore \left( 1 \residuum y \right) = y
            \end{align*}

            \item Let $y = 1$:
            \begin{align*}
                z \leq \left( x \residuum 1 \right) 
                    & \iff \underbrace{ \left( x \residuum z \right) \leq 1}_{
                    \text{always true in} \ \fuzzyDomain} \\
                \therefore  \left( x \residuum 1 \right) = 1
            \end{align*}
            
            \item Let $z = 1$:
            \begin{align*}
                1 \leq \left( x \residuum y \right) 
                    & \iff x = \left( x \residuum 1 \right) \leq y \\
                \therefore  \left( x \residuum y \right) = 1 
                    & \iff x \leq y
            \end{align*}
        \end{itemize}
    \end{proof}
\end{lemma}

\begin{lemma}
    Every continuous t-norm $\tnorm$ has a unique residuum which satisfies
    \begin{align*}
        \forall x, y \in \fuzzyDomain: & \\
        x \residuum y &= \max\set{z \rvert x \tnorm z \leq y}
    \end{align*}

    \begin{proof}
        Let 
        \begin{equation}
            x \residuum y \definedas \sup\set{z \rvert x \tnorm z \leq y}
        \end{equation}
        Assuming, $ x \tnorm z \leq y $ holds, 
        it implies $z \leq x \residuum y$ 
        by the definition of the supremum.
        It has to be shown that the supremum actually is the maximum,
        which means that 
        $x \tnorm \left( x \residuum y\right) \leq y$ has to hold.
        \begin{align*}
            x \tnorm \left( x \residuum y \right) 
                &= x \tnorm \sup\set{z \rvert x \tnorm z \leq y} \\
                &= \sup\set{x \tnorm z \rvert x \tnorm z \leq y}
                    \sidenote{by Lemma \ref{lemma-tnorm-continuous}} \\
                &\leq y
        \end{align*}
        Assume that $\tnorm$ has another residuum $\rightsquigarrow$.
        \begin{alignat*}{3}
                &&\underbrace{x \rightsquigarrow y }_z 
                    &\leq x\rightsquigarrow y \\
            \iff&& x \tnorm \underbrace{\left( x \rightsquigarrow y \right)}_z 
                    &\leq y \\
            \iff&& \underbrace{x \rightsquigarrow y }_z
                    &\leq x \residuum y 
        \end{alignat*}
        Similarly the reverse direction 
        $x \residuum y \leq x \rightsquigarrow y$
        can be shown.
        Therefore, $\residuum$ and $\rightsquigarrow$ have to be equal.
    \end{proof}
\end{lemma}

\begin{statement}
    For every contiuous t-norm  the \emph{residuum} is compatible 
    with the \emph{implication} of propositional logic 
    for arguments $x, y \in \set{0, 1}$.
    % TODO proof
\end{statement}

\begin{definition}[Precomplement]{def-tnorm-precomplement}
    The \textbf{precomplement} $\precomplementX$ of a t-norm $\tnormX$ 
    is a unary operator
    \begin{equation*}
        \precomplementX: \fuzzyDomain \to \fuzzyDomain
    \end{equation*}
    such that
    \begin{equation*}
        \precomplementX x \definedas x \residuumX 0
    \end{equation*}
\end{definition}

\begin{statement}
    For every contiuous t-norm  the \emph{precomplement} is compatible 
    with the \emph{negation} of propositional logic 
    for arguments $x, y \in \set{0, 1}$.
    
    % TODO proof
\end{statement}

\begin{definition}[Co-Norm]{def-tnorm-conorm}
    The \textbf{triangular co-norm} $\tconormX$ of a t-norm $\tnormX$ 
    is a binary operator
    \begin{equation*}
        \tconormX: \fuzzyDomain \times \fuzzyDomain \to \fuzzyDomain
    \end{equation*}
    such that
    \begin{equation*}
        x \tconormX y \definedas 1 - \left( 
            \left( 1 - x \right) \tnormX \left( 1 - y \right)
        \right)
    \end{equation*}
\end{definition}

\begin{statement}
    Each triangular co-norm $\tconorm$ is automatically 
        \emph{Associative},
        \emph{Commutative},
        \emph{Monotone},
    and has the \emph{unit element 0}.
\end{statement}


\begin{statement}
    For every contiuous t-norm  the \emph{co-norm} is compatible 
    with the \emph{disjunction} of propositional logic 
    for arguments $x, y \in \set{0, 1}$.
    
    % TODO proof
\end{statement}

% TODO proof ex 1.2 c)
% TODO statement 1.3 a)
% TODO statement + proof 1.3 b)
