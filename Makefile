# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# general

.PHONY: all clean

all: latex

clean: latex-clean

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# latex

LATEX_MAIN_FILE := main.tex

.PHONY: latex latex-continuous latex-clean

latex:
	latexmk -pdf ${LATEX_MAIN_FILE}

latex-continuous:
	latexmk -pdf -pvc -interaction=nonstopmode ${LATEX_MAIN_FILE}

latex-clean:
	latexmk -C
