gridx = gridy = linspace(0, 1, 21)
[xx, yy] = meshgrid(gridx, gridy)
tnormp = xx .* yy

#fx = @(t) t;
#fy = @(t) t;
#fz = @(t) t.*t;

colormap("rainbow")
img = figure()
surf(gridx, gridy, tnormp)
#hold on
#lplot = ezplot3 (fx, fy, fz, [0, 1], 21)
#set(lplot, 'color', 'k')

# also works with tikz instead of eps 
# but generates an enormous file
print(img, "../images/tnormp.eps", "-depsc")