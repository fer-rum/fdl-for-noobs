gridx = gridy = linspace(0, 1, 21)
[xx, yy] = meshgrid(gridx, gridy)
tnorml = max(xx + yy - 1, 0)

colormap("rainbow")
img = figure()
surf(gridx, gridy, tnorml)
line([0 0.5],[0 0.5],[0 0.5])
line([1 0.5],[1 0.5],[1 0.5])

# also works with tikz instead of eps 
# but generates an enormous file
print(img, "../images/tnorml.eps", "-depsc")