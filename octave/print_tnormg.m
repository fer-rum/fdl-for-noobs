gridx = gridy = linspace(0, 1, 21)
[xx, yy] = meshgrid(gridx, gridy)
tnormg = min(xx, yy)

colormap("rainbow")
img = figure()
surf(gridx, gridy, tnormg)
line([0 1],[0 1],[0 1])

# also works with tikz instead of eps 
# but generates an enormous file
print(img, "../images/tnormg.eps", "-depsc")