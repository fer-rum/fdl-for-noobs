# FDL for Noobs

An attempt to make __Fuzzy Description Logic__ accessible for people with limited cerebral capacity (like myself).

## Current version of PDF
The version automatically generated using the latest commit to the master branch is available [here](https://gitlab.com/fer-rum/fdl-for-noobs/-/jobs/artifacts/master/raw/main.pdf/?job=build-job
).

## License
Licensed under CC-BY-NC-SA 4.0
See either [LICENSE.md](LICENSE.md) for full text or [here](https://creativecommons.org/licenses/by-nc-sa/4.0/) for human-readable digest.

## LaTeX - Symbols
Some additional commands were defined to ease the writing of clunky maths:
All symbols need to be in a math-environment.

### General Math - Symbols

* `\domain` prints the symbol for the fuzzy logic domain ([0,1]).
* `\naturalnums` prints the symbol for natural numbers.
* `\range{x}` is an abbreviation for writing a range with contents _x_.
* `\set{x}` is an abbreviation for writing a set with contents _x_.
* `\valuation{x}` prints the expression of applying a valuation to the propositional logic formula _x_.
* `\sidenote{x}` prints a vertical bar followed by _x_ as non-math text. Useful for comments in _align_ environments.
* `\iff` is an override that prints the text _iff_ with proper spaces instead of the arrow version LaTeX uses by default.
* `\definedas`  prints a proper definition symbol (:=)

### Symbols for T-Norms
Each tnorm-related symbol exists in a plain variant, and variations
* **G** for Gödel
* **P** for Product
* **L** for Lucasiewitch
* **Z** for Zadeh
* **X** for arbitrary

The following tnorm-related symbols are defined
* `\tnorm` prints the Symbol for a triangular norm
* `\residuum` prints the residuum symbol
* `\precomplement` prints the precomplement symbol
* `\conorm` prints the co-norm symbol
